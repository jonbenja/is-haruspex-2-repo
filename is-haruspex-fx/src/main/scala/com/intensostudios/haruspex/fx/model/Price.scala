package com.intensostudios.haruspex.fx.model

import java.time.LocalDateTime

/**
  * Created by jwright on 18/08/16.
  */
case class Price(val price:BigDecimal,val dt:LocalDateTime) {

}
