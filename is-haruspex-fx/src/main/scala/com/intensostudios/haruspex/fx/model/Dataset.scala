package com.intensostudios.haruspex.fx.model

import FxDefs._

/**
  * Created by jwright on 18/08/16.
  */
case class Dataset(pair: Pair, timeframe: TimeFrame, prices: Seq[Price]) {

}
