package com.intensostudios.haruspex.fx.model

/**
  * Created by jwright on 18/08/16.
  */
object FxDefs {

  sealed trait TimeFrame
  case object Minutely extends TimeFrame
  case object Hourly extends TimeFrame
  case object Daily extends TimeFrame


  sealed trait Pair
  case object GBPUSD extends Pair
  case object EURUSD extends Pair
  case object USDJPY extends Pair


  sealed trait Direction
  case object Long extends Direction
  case object Short extends Direction


}
